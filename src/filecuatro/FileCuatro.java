/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filecuatro;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class FileCuatro {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) throws IOException {
    System.out.println("Introduce la ruta absoluta de la carpeta dentro de la que quieres buscar");
    Scanner input = new Scanner(System.in);
    File folder = new File(input.next());
    System.out.println("Introduce el nombre del fichero que quieres buscar");
    String fileName = input.next();
    File result = fetchFile(folder,fileName);
    if (result!=null){
      System.out.println("Fichero encontrado: "+result.getCanonicalPath());
    }else{
      System.out.println("Fichero no encontrado");
    }
  }
  
  /**
   * Función recursiva para buscar un archivo dentro de una carpeta
   * @param directory File con la referencia a la carpeta en la que se va a buscar
   * @param fileName String con el nombre del archivo a encontrar
   * @return El archivo buscado o null si no fue encontrado
   */
  private static File fetchFile(File directory, String fileName) {
    System.out.println("Buscando en la carpeta "+directory.getAbsolutePath());
    //Declaramos un array de archivos con el contenido de la carpeta
    File [] childFiles = directory.listFiles();
    //Declaramos una variable file para guardar el archivo inicializada a null
    File fetchedFile = null;
    //Recorremos los arvhivos dentro de la carpeta
    for (File childFile : childFiles) {
      System.out.println("Comparando "+childFile.getName()+" con "+fileName);
      //Si el archivo se llama como el que buscamos
      if (childFile.getName().equals(fileName)){
        System.out.println("ENCONTRADO!!1!!!1!");
        //devolvemos el archivo
        return childFile;
      }else if (childFile.isDirectory()){
        //Si no se llama igual y es una carpeta, volvemos a llamar a la funcion, guardando el 
        //resultado de esta en una variable auxiliar de tipo fichero
        File aux = fetchFile(childFile, fileName);
        //Si el resultado de buscar dentro de esa carpeta no es null, igualamos la variable
        //que vamos a devolver al archivo obtenido de llamar de nuevo a fetchFile
        if (aux!=null) fetchedFile = aux;
      }
    }  
    return fetchedFile;
  }
  
  
  
  }

